from smd import data
from smd.io import write_midi


if __name__ == '__main__':

    print(data.resolution)
    print(data.columns)

    set = 'jsb'
    split = 'test'
    data_tuples = data.load_tuples(set, split)
    tempo_dict = data.load_tempos(set, split)
    assert len(data_tuples) == len(tempo_dict)

    for i in range(len(data_tuples)):
        print(tempo_dict[i])
        _, t0s, t1s, midis, part_indices = zip(*data_tuples[i])
        print(write_midi(t0s, t1s, midis, part_indices, '/tmp/test_%.4i.mid' % i, tempo_dict[i]))
