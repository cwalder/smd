import os.path
import smd

_data_path = os.path.expanduser(os.path.dirname(smd.__path__[0])) + '/data/'
smd_path = '%sSymbolicMusicMidiDataV1.1' % _data_path
_download_command = 'cd %s\n./download.sh' % _data_path

assert os.path.exists(smd_path), ('data not found. run these two commands:\n%s' % _download_command)

print(smd_path)
