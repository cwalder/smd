from os.path import expanduser
from smd import data
import smd.midi as python_midi


def write_midi(t0s, t1s, midis, part_indices, filename, tempo=None, resolution=None, velocity=None, end_of_track_ticks=None):

    if resolution is None:
        resolution = data.resolution

    if end_of_track_ticks is None:
        end_of_track_ticks = 2 * resolution
        
    if tempo is None:
        tempo = 120

    filename = expanduser(filename)
    pattern = python_midi.Pattern(tick_relative=False, resolution=resolution)

    t = python_midi.SetTempoEvent(tick=0)
    t.set_bpm(tempo)
    track = python_midi.Track()
    pattern.append(track)
    track.append(t)
    eot = python_midi.EndOfTrackEvent(tick=0)
    track.append(eot)

    if velocity is None:
        velocity = [100] * len(t0s)

    for thispart in sorted(set(part_indices)):
        track = python_midi.Track()
        pattern.append(track)
        tprev = 0
        non, noff = 0, 0
        for t, part, onset, midi, vel in sorted(zip(t0s + t1s, part_indices * 2, [True] * len(midis) + [False] * len(midis), midis * 2, velocity * 2)):
            # t = int(t * 60. / tempo)
            # print t, part, onset, midi, vel
            assert midi >= 0 and midi < 128, midi
            if part == thispart:
                tick = t - tprev
                pitch = midi
                if onset:
                    non += 1
                    event = python_midi.NoteOnEvent(tick=int(tick), velocity=vel, pitch=int(pitch))
                else:
                    noff += 1
                    event = python_midi.NoteOffEvent(tick=int(tick), pitch=int(pitch))
                track.append(event)
                tprev = t
        eot = python_midi.EndOfTrackEvent(tick=end_of_track_ticks if thispart == 0 else 1)
        # eot = python_midi.EndOfTrackEvent(tick=1)
        track.append(eot)
    python_midi.write_midifile(filename, pattern)
    pattern2 = python_midi.read_midifile(filename)
    assert list(map(len, pattern)) == list(map(len, pattern2))
    return filename
