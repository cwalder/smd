import gzip
from smd import paths
import pandas as pd
import numpy as np

dataset_names = ['jbm', 'jsb', 'pmd', 'not', 'mus', 'cma']
split_names = ['train', 'test', 'valid']
columns = ('file_index', 't0', 't1', 'midi', 'part')
column2index = {k: v for v, k in enumerate(columns)}
FILE_INDEX = column2index['file_index']
T0 = column2index['t0']
T1 = column2index['t1']
MIDI = column2index['midi']
PART = column2index['part']

_filename = '%s/%s_%s.csv'
_tempo_filename = '%s/%s_%s_tempo.csv'
tempo_columns = ('file_index', 'tempo')


with open('%s/resolution.txt' % paths.smd_path, 'r') as fp:
    resolution = int(fp.readline().strip())


def _load_df(dataset='jsb', split='train', tempo=False):
    assert dataset in dataset_names, dataset
    assert split in split_names, split
    if tempo:
        fn = _tempo_filename
        cols = tempo_columns
    else:
        fn = _filename
        cols = columns
    filename = fn % (paths.smd_path, dataset, split)
    for opener in (open, gzip.open):
        try:
            with opener(filename + ('.gz' if opener == gzip.open else '')) as fp:
                df = pd.read_csv(fp, dtype={'file_index': int, 't0': int, 't1': int, 'midi': int, 'part': int})
            break
        except:
            pass
    assert tuple(df.columns) == cols
    return df


def _load_np_generator(*args, **kwargs):
    df = _load_df(*args, **kwargs)
    mat = df.values
    difft = np.diff(df.file_index)
    assert np.all(difft >= 0)
    inds = np.concatenate(([0], np.where(difft == 1)[0] + 1, [mat.shape[0]]))
    return (mat[i:j, :] for i, j in zip(inds, inds[1:]))


def load_tuples(*args, **kwargs):
    '''
    Example:
    t = load_tuples("mus", "test")
    len(t) = number of files
    len(t[0]) = number of notes in the first file
    t[0][0] = the first event of the first file, with values corresponding to smd.data.columns
    '''
    return [tuple(zip(*this_mat.T)) for this_mat in _load_np_generator(*args, **kwargs)]


def load_tempos(*args, **kwargs):
    '''
    Example:
    d = load_tempos("mus", "train")
    '''
    return dict([_[0] for _ in load_tuples(*args, **kwargs, tempo=True)])
